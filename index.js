require('dotenv').config()
const express = require('express')
const path = require('path')
const cors = require('cors')
const app = express()

const redirectRoute = require('./routes/redirect')
const shortenRoute = require('./routes/shorten')
const mongoose = require('mongoose')
const PORT = process.env.PORT || 4000

app.enable('trust proxy')
app.use(cors())
app.use(express.json())

app.use(express.static(path.join(__dirname, 'client/public')))

app.use('/', redirectRoute)
app.use('/api/url', shortenRoute)
mongoose.connect(
	'mongodb+srv://awran5:HAZdvTn3zzzG9A%2D@devcluster-vuzfw.mongodb.net/shortener_url?retryWrites=true&w=majority',
	{ useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true }
)

app.listen(PORT, () => console.log(`server is started at port ${PORT}`))
