import React from 'react'
import PropTypes from 'prop-types'
import './layout.css'

const Layout = ({ children }) => {
	return (
		<div className="App d-flex flex-column vh-100 justify-content-between">
			<main>{children}</main>
			<footer className="small text-muted footer mb-2">
				<div className="footer-info text-center">
					<ul className="list-inline">
						<li className="list-inline-item">v1.0.0</li>
						<li className="list-inline-item"> - </li>
						<li className="list-inline-item">
							<a
								href="https://github.com/awran5/shorten/"
								target="_blank"
								title="GitHub"
								rel="noopener noreferrer"
							>
								GitHub
							</a>
						</li>
						<li className="list-inline-item"> - </li>
						<li className="list-inline-item">
							<a
								href="https://github.com/awran5/shorten/issues"
								target="_blank"
								title="Issues"
								rel="noopener noreferrer"
							>
								Issues
							</a>
						</li>
					</ul>
					<div className="copyright">
						© {new Date().getFullYear()}. Designed and built with ❤️ by the
						<a href="https://gkstyle.net/" title="GK STYLE">
							{' '}
							GK STYLE{' '}
						</a>
						team.
					</div>
				</div>
			</footer>
		</div>
	)
}

Layout.propTypes = {
	children: PropTypes.node.isRequired
}

export default Layout
