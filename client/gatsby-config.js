const proxy = require('http-proxy-middleware')
module.exports = {
	siteMetadata: {
		title: `Free URL Shortener Tool`,
		description: `Shorten is Free URL shortener tool to shorten a long URL and reduce its length. Use our URL
		Shortener to create a trusted shortened links and share them with your family and friends.`,
		author: `@gkstylegroup`
	},
	developMiddleware: app => {
		app.use(
			'/api/url/shorten/',
			proxy({
				target: 'http://localhost:4000'
			})
		)
	},
	plugins: [
		`gatsby-plugin-react-helmet`,
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `gatsby-starter-default`,
				short_name: `starter`,
				start_url: `/`,
				background_color: `#663399`,
				theme_color: `#663399`,
				display: `minimal-ui`,
				icon: `src/images/gatsby-icon.png` // This path is relative to the root of the site.
			}
		}
		// this (optional) plugin enables Progressive Web App + Offline functionality
		// To learn more, visit: https://gatsby.dev/offline
		// `gatsby-plugin-offline`,
	]
}
